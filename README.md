README
======

Diese Präsentation wurde als Asciidoc - Dokument ersteltt und
wird mittells "asciidoctor" und "revealjs" in eine HTML5-Präsentation konvertiert.

Diese Konvertierung wird durch ausführen des entsprechenden Docker-Images durchgeführt

    ./run-docker-image.sh

Dieser Script konvertiert und ruft danach Google-Chrome mit der Präsentation auf.
Die Container-Instance wacht dann über das Verzeichnis "slides" und startet die Konvertierung automatisch bei jeder Änderung der .adoc-Dateien in diesem Verzeichnis.

Das konvertierte Ergebnis wird unter "slides/html5" abgelegt.
